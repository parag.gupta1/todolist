
    let cardList = document.getElementById("card-list"); 
    let itemInput = document.getElementById("item-input");
    let itemAdd = document.getElementById("item-add");

    itemAdd.addEventListener("click", add);
    let itemArray = [];
    function add() {
        if(itemInput.value=="")
        {
            alert("Add value");

        }
        else{
            let item = {};
            item.itemText = itemInput.value;
            item.itemStatus = "incomplete";
            itemArray.push(item);
            removeAll();
            showAll();
            itemInput.value="";
        }

    }

    function removeAll(argument) {
        let list = document.querySelectorAll("#card-list li");
        for (var i = 0; i < list.length; i++) {
            cardList.removeChild(list[i]);
        };

    }

    function showAll(argument) {

        for (var i = 0; i < itemArray.length; i++) {

            var li = document.createElement("li");
            let itemCheck = document.createElement("input")
            itemCheck.type = "checkbox";
            if (itemArray[i]["itemStatus"] == "completed") {
                li.style.textDecoration = "line-through";
                itemCheck.checked = "true";

            }
            // else{

            // }
            let itemDelete = document.createElement('input');
            itemDelete.type = "button"
            itemDelete.value = "delete";
            let item = document.createElement("a");
            item.href = "#";
            let itemHeading = document.createElement("p");
            itemHeading.innerText = itemArray[i]["itemText"];
            item.appendChild(itemHeading);

            li.appendChild(itemCheck);
            li.appendChild(item);
            li.appendChild(itemDelete);
            li.appendChild(document.createElement("br"));

            cardList.appendChild(li);

        };
    }

    cardList.addEventListener('click', function (ev) {
        if (ev.target.type === 'checkbox') {
            ev.target.classList.toggle('checked');
            let itemArr = document.querySelectorAll("#card-list input[type=checkbox]");
            var index = [].indexOf.call(itemArr, ev.target);
            console.log(ev)
            if (ev.target.checked) {
                itemArray[index].itemStatus = "completed";
            } else {
                itemArray[index].itemStatus = "incompleted";
            }

        } else if (ev.target.type == "button") {
            let itemArr = document.querySelectorAll("#card-list input[type=button]");
            var index = [].indexOf.call(itemArr, ev.target);
            itemArray.splice(index, 1);
        }
        removeAll();
        showAll();
    }, false);
